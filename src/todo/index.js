import React, { useState, useEffect } from "react";

import { BiAddToQueue } from "react-icons/bi";
import data from "./todo.json";

import {
  Button,
  Table,
  TextField,
  CheckBox,
  Grid,
  Row,
  InputField,
} from "ui-framework-bootstrap";

const Todo = () => {
  const [todoItems, setTodoItems] = useState([]);

  useEffect(() => {
    setTodoItems(data);
  }, [data]);
  const [todoItemsCheck, setTodoItemsCheck] = useState("");

  function onSubmit(e) {
    e.preventDefault();
    if (todoItemsCheck !== "") {
      setTodoItems([
        ...todoItems,
        {
          id: todoItems.length + 1,
          todo: todoItemsCheck,
          checked: false,
        },
      ]);
      setTodoItemsCheck("");
    } else {
      alert("No");
    }
  }

  const theadData = [
    { title: "S.N", type: "sn", isSort: true },
    { title: "Todo Item", type: "todo", isSort: true },
  ];

  function handleEdit(id) {
    alert(id);
  }

  function handleDelete(id) {
    setTodoItems(
      todoItems.filter((x) => {
        return x.id !== id;
      })
    );
  }

  return (
    <>
      <div className="container border mt-2 p-3">
        <div className="text-center fw-bold my-2">Todo List</div>
        <form onSubmit={onSubmit}>
          <Grid className="justify-content-center">
            <Row className="col-12">
              <TextField
                width="lg"
                placeHolder="Input todo here...."
                onChange={(e) => setTodoItemsCheck(e.target.value)}
                value={todoItemsCheck}
                focus
                required
                validator={todoItemsCheck}
                helpertext="Enter Todo"
              />
            </Row>
            <Row className="col-12">
              <Button
                type="submit"
                style={{ marginTop: "10px" }}
                text="Add Todo"
                size="sm"
                width="lg"
                icon={<BiAddToQueue />}
                outline={true}
              />
            </Row>
          </Grid>
        </form>
      </div>
      <div className="container border">
        <Grid className="justify-content-center">
          <Row className="col-md-6">
            <center className="mt-3">
              <strong>Remaining</strong>
            </center>

            <div className="container my-3">
              {todoItems?.filter((elm, i) => elm.checked === false).length > 0
                ? todoItems
                    ?.filter((elmX) => elmX.checked === false)
                    .map((elm, i) => (
                      <div className=" border p-2 container my-3 d-flex align-items-center justify-content-around">
                        <CheckBox
                          onChange={() => {
                            setTodoItems(
                              [...todoItems].map((object) => {
                                if (object.id === elm.id) {
                                  return {
                                    ...object,
                                    checked: !object.checked,
                                  };
                                } else return object;
                              })
                            );
                          }}
                        />
                        {/* <span className="mt-3 mx-2"> {elm.todo}</span> */}
                        <InputField
                          className="me-2"
                          width="lg"
                          value={elm.todo}
                          placeHolder="Edit todo here..."
                          onChange={(e) => {
                            setTodoItems(
                              [...todoItems].map((object) => {
                                if (object.id === elm.id) {
                                  return {
                                    ...object,
                                    todo: e.target.value,
                                  };
                                } else return object;
                              })
                            );
                          }}
                        />
                        <Button
                          background="danger"
                          type="submit"
                          style={{ marginTop: "10px" }}
                          text="Delete"
                          size="md"
                          width="sm"
                          icon={<BiAddToQueue />}
                          onClick={() => handleDelete(elm.id)}
                          className="mx-2"
                        />
                      </div>
                    ))
                : "No Remaining Task Found"}
            </div>
          </Row>
          <Row className="col-md-6">
            <center className="mt-3">
              <strong>Finished</strong>
            </center>
            {todoItems?.filter((x) => x.checked === true).length > 0 ? (
              <div className="container my-3">
                <Table
                  theadData={theadData}
                  tbodyData={todoItems
                    ?.filter((x) => x.checked === true)
                    .map((elm, indexx) => {
                      return {
                        sn: indexx + 1,
                        ...elm,
                      };
                    })}
                  bordered
                  isDelete
                  handleEdit={handleEdit}
                  handleDelete={handleDelete}
                />
              </div>
            ) : (
              "No  Finished Task Found"
            )}
          </Row>
        </Grid>
      </div>
    </>
  );
};

export default Todo;
